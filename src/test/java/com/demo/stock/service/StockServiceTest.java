package com.demo.stock.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.demo.stock.entity.Stock;
import com.demo.stock.exception.StockNotFoundException;
import com.demo.stock.repository.StocksRepository;

public class StockServiceTest {

	@InjectMocks
	StockService stockService;

	@Mock
	StocksRepository stocksRepository;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test

	public void testGetStocks() throws StockNotFoundException {

		List<Stock> stocks = stocksRepository.findAll();
		Stock stock = new Stock();
		stock.setStockId(1);
		Stock stock1 = new Stock();
		stock1.setStockId(2);
		Stock stock2 = new Stock();
		stock2.setStockId(3);
		Stock stock3 = new Stock();
		stock3.setStockId(4);
		stocks.add(stock);
		stocks.add(stock1);
		stocks.add(stock2);
		stocks.add(stock3);
		Mockito.when(stocksRepository.findAll()).thenReturn(stocks);
		List<Stock> stockslist = stockService.getStocks();
		assertEquals(4, stockslist.size());

	}

	public void testGetStockByid() throws Exception {

		Stock stock = new Stock();
		stock.setStockId(1);
		stock.setStockName("Aditya Birla");
		stock.setQuantity(5);
		stock.setStockPrice(1500.00);
		stock.setTrend("Good");
		stock.setCreatedDate(LocalDate.now());
		Mockito.when(stocksRepository.findById(1)).thenReturn(Optional.of(stock));
		Stock stock1 = stockService.getStocksById(1);
		assertEquals(1500, stock1.getStockPrice());

	}

}
