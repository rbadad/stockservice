package com.demo.stock.controller;

import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.demo.stock.dto.StockResponseDTO;
import com.demo.stock.entity.Stock;
import com.demo.stock.service.StockService;

public class StockControllerTest {

	@InjectMocks
	private StockController stockController;

	@Mock
	private StockService stockService;

	@BeforeEach
	public void setups() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getStockstest() {
		List<Stock> stocks = new ArrayList<Stock>();
		Stock stock = new Stock();
		stock.setStockId(1);
		stock.setStockName("TATA");
		stock.setStockPrice(123.123);
		stocks.add(stock);
		try {
			doReturn(stocks).when(stockService).getStocks();
			stockController.getStocks();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getStockByIdtest() {
		Stock sdto = new Stock();
		sdto.setStockId(1);
		sdto.setQuantity(123);
		sdto.setStockName("qwerty");
		sdto.setStockPrice(123.123);
		sdto.setTrend("good");
		try {
			doReturn(sdto).when(stockService).getStocksById(1);
			stockController.getStockById(1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
