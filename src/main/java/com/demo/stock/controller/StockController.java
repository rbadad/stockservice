package com.demo.stock.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.stock.entity.Stock;
import com.demo.stock.exception.StockNotFoundException;
import com.demo.stock.service.StockService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/stocks")
public class StockController {
	private static final Logger LOGGER = LoggerFactory.getLogger(StockController.class);
	@Autowired
	private StockService stockService;

	@GetMapping
	@ApiOperation("Stocks Information")
	public ResponseEntity<List<Stock>> getStocks() throws StockNotFoundException {
		LOGGER.debug("Stocks Controller :: start");
		List<Stock> stocks = stockService.getStocks();
		return new ResponseEntity<>(stocks, HttpStatus.OK);
	}

	@ApiOperation("stock details by id")
	@GetMapping("/{stockId}")
	public ResponseEntity<Stock> getStockById(@PathVariable("stockId") Integer stockId) throws StockNotFoundException {
		LOGGER.info("Inside getStockById method");
		Stock stock = stockService.getStocksById(stockId);
		return new ResponseEntity<>(stock, HttpStatus.OK);
	}

}
