package com.demo.stock.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.stock.entity.Stock;
import com.demo.stock.exception.StockNotFoundException;
import com.demo.stock.repository.StocksRepository;

@Service
public class StockService {

	@Autowired
	StocksRepository stocksRepository;

	public List<Stock> getStocks() throws StockNotFoundException {
		List<Stock> stocks = stocksRepository.findAll();
		return stocks;
	}

	public Stock getStocksById(Integer stockId) throws StockNotFoundException {
		Optional<Stock> stock = stocksRepository.findById(stockId);
		if (stock.isPresent()) {
			return stock.get();
		} else {
			throw new StockNotFoundException("Stock Not Found");
		}
	}
}