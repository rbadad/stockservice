package com.demo.stock.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.demo.stock.constants.AppConstants;

@RestControllerAdvice
public class GlobalExceptionHandler {

	
	@ExceptionHandler(value = StockNotFoundException.class)
	public ResponseEntity<String> handleStockNotFoundException() {
		return new ResponseEntity<>(AppConstants.STOCK_NOT_FOUND,HttpStatus.OK);
	}
	
	
	
}
